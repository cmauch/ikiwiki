#!/usr/bin/perl 
# -*- Mode: perl; tab-width: 4; indent-tabs-mode: nil; -*-
#
# Pingback Proxy: XML-RPC POST to HTTP GET with referrer
#
# Copyright (c) 2002 by Ian Hickson
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

use diagnostics;
use lib $ENV{OPENSHIFT_HOMEDIR} . '/perl-5.10/perl5lib/lib/perl5';

use LWP::UserAgent;
use RPC::XML;
use RPC::XML::Parser;
use URI;
use URI::Escape;
use HTTP::Request::Common qw(GET);
use HTML::Entities;

my $wikibase = $ENV{OPENSHIFT_DATA_DIR} . '/WikiText/';
my $pingbase = $ENV{OPENSHIFT_DATA_DIR} . '/Pingbacks/';
my $logfile  = $ENV{OPENSHIFT_DATA_DIR} . '/pingbacks.log';

if ( $ENV{'REQUEST_METHOD'} ne 'POST' ) {
    pingresult(
        '405 Method Not Allowed',
        -32300, 'Only XML-RPC POST requests recognised.',
        'Allow: POST'
    );
}

if ( $ENV{'CONTENT_TYPE'} ne 'text/xml' ) {
    pingresult( '415 Unsupported Media Type',
        -32300, 'Only XML-RPC POST requests recognised.' );
}

# get the data
local $/ = undef;
my $input = <STDIN>;

# parse it
my $parser  = RPC::XML::Parser->new();
my $request = $parser->parse($input);
if ( not ref($request) ) {
    result( '400 Bad Request', -32700, $request );
}

# handle it
my $name      = $request->name;
my $arguments = $request->args;
if ( $name ne 'pingback.ping' ) {
    result( '501 Not Implemented', -32601, "Method $name not supported" );
}
if ( @$arguments != 2 ) {
    result(
        '400 Bad Request',
        -32602,
        "Wrong number of arguments (arguments must be in the form 'from', 'to')"
    );
}
my $source = $arguments->[0]->value;
my $target = $arguments->[1]->value;

# pass it on to the real server
my $ua = LWP::UserAgent->new();
$ua->agent( $ua->agent . ' (Hixie\'s pingback proxy)' );
$ua->timeout(5);
$ua->env_proxy();
$ua->protocols_allowed( [ 'http', 'https' ] );
my $get = HTTP::Request->new( 'GET', $target );
$get->header( 'Referer' => $source );
my $response = $ua->request($get);

if ( $response->is_error ) {
    if ( $response->code == 404 ) {
        result( '502 Bad Gateway', 0x0032, $response->status_line );
    }
    else {
        result( '502 Bad Gateway', 0x0032, $response->status_line );
    }
}

logdata( $source, $target, $wikibase );
pingresult( '200 OK', 0, $response->status_line );

sub pingresult {
    my ( $status, $error, $data, $extra ) = @_;
    my $response;
    if ($error) {
        $response =
          RPC::XML::response->new( RPC::XML::fault->new( $error, $data ) );
    }
    else {
        $response = RPC::XML::response->new( RPC::XML::string->new($data) );
    }
    print "Status: $status\n";
    if ( defined($extra) ) {
        print "$extra\n";
    }
    print "Content-Type: text/xml\n\n";
    print $response->as_string;
    exit;
}

sub logdata {
    my $source  = shift;
    my $target  = shift;
    my $base    = shift;
    my $logdate = `TZ="America/Los_Angeles" date '+%A, %B %m %Y at %I:%M %p %Z'`;
    $logdate =~ s/\n//g;

    my $titled_link;    # Fully Formatted Markdown Pingback Link
    my $wikifile;       # Wiki Node being Referenced
    my $pingfile;       # Wiki Node Pingback File to create or add to
    my $pingpath;       # Path to Pingback File
    my $selflink = 1;   # If Node can't be found, we write to Global Pingback File

    my $wikitarget = $target;
    my $rawtarget  = $target;

    # Strip out http stuff, lets get the Wikinode in Markdown and Raw
    $wikitarget =~ s/http:\/\/point-defiance.rhcloud.com(.*)\//\[\[$1\]\]/g;
    $rawtarget =~ s/http:\/\/point-defiance.rhcloud.com\/(.*)\//$1/g;

    # Slurp Title from Source website
    $titled_link = &gettitle($source);
    if ( $titled_link eq $source ) {
        $titled_link = "\<$source\>";
    }

    # Set variables we work with
    my $wiki1 = "${wikibase}${rawtarget}.mdwn";
    my $ping1 = "${pingbase}${rawtarget}\/pingback.mdwn";
    my $path1 = "${pingbase}${rawtarget}\/";

    my $wiki2 = "${wikibase}${rawtarget}\/index.mdwn";
    my $ping2 = "${pingbase}${rawtarget}\/pingback.mdwn";
    my $path2 = "${pingbase}${rawtarget}\/";

    my $wiki3 = "${wikibase}OddPings.mdwn";
    my $ping3 = "${pingbase}OddPings.mdwn";

    if ( -e $wiki1 ) {    # File exists eg: WikiDir/File.mdwn
        $wikifile = $wiki1;
        $pingfile = $ping1;
	$pingpath = $path1;
        $selflink = 0;
    }
    elsif ( -e $wiki2 ) { # File exists eg: WikiDir/File/index.mdwn
        $wikifile = $wiki2;
        $pingfile = $ping2;
	$pingpath = $path2;
        $selflink = 0;
    }
    else {		  # Oddness in extreme!
        $wikifile = $wiki3;
        $pingfile = $ping3;
	$pingpath = ${pingbase};
    }

    # Strip out extra slashes
    $wikifile =~ s/\/\//\//g;
    $pingfile =~ s/\/\//\//g;
    $pingpath =~ s/\/\//\//g;

    open( LOGFILE, ">>$logfile" ) || die("Cannot Open $logfile");
    print LOGFILE "-------------------------------------------------------------------------------\n";
    print LOGFILE "Pingback on $logdate:\n\tWikiNode:\t$rawtarget\n\tFrom:\t\t$source\n\tUnderlay:\t$pingfile\n";
    close LOGFILE;

    # First, create path if needed
    if ( ! -d $pingpath ) { 
        `mkdir -p $pingpath`;  
    }
    
    # Write the pingback to underlay file 
    open( HANDLE, ">>$pingfile" ) || die("Cannot Open $pingfile");
    if ($selflink) {
        print HANDLE "\n* Referenced by: $wikitarget : $titled_link on \<span clas=\"pingbackdate\"\>$logdate\<\/span\>\n";
    }
    else {
        print HANDLE "\n* Referenced by: $titled_link on \<span class=\"pingbackdate\"\>$logdate\<\/span\>\n";
    }
    close HANDLE;

    exec( "$ENV{OPENSHIFT_DATA_DIR}refreshwiki.sh" );
}

sub gettitle {
    my $target = shift;

    my $ua2 = LWP::UserAgent->new;
    $ua2->agent('PD Pingback Title Scraper 0.1');

    # Request object
    my $req = GET $target;

    # Make the request
    my $res = $ua2->request($req);

    # Check the response
    if ( $res->is_success ) {
        my $title = &striptitle( $res->content );
        return "\[$title\]\($target\)";
    }
    else {
        return $target;
    }
}

sub striptitle {
    my $content = shift;
    return 0 if !$content;
    $content =~ m{<title>(.*)<\/title>};
    my $stripped = encode_entities($1);
    return $stripped;
}
